<?php
/**
 * Plugin Name: MF Probenplan
 * Version: 1.0
 * Author: Sebastian Schendel
 * Author URI: http://www.sebiworld.net
 * Description: Verwaltung von Probenterminen und Erstellung von Probenplänen
 */

// Set path-constants:
define('MF_PROBENPLAN_DIR', dirname(__FILE__));
define('MF_PROBENPLAN_URL', WP_PLUGIN_URL . '/mf-probenplan/');

class MF_Probenplan {

	/**
	* Constructor, called on plugin's initialization
	*/
	function MF_Probenplan () {
		// Translation, CSS and js: 
		add_action( 'init', array($this, 'load_plugin_textdomain' ));
		add_action('admin_enqueue_scripts', array($this,'load_admin_stylesheets'));
		add_action('admin_enqueue_scripts', array($this,'load_admin_scripts'));

		// Create the custom post type (mf_probe):
		add_action( 'init', array( $this, 'register_probe_post_type' ));

		//Create custom taxonomy:
		add_action( 'init', array( $this, 'create_rehearsalcategory_taxonomy' ));

		add_action( 'admin_menu', array( $this, 'register_probenplan_meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save_probenplan_meta_boxes' ) );

		add_filter("manage_edit-mf_probe_columns", array( $this, "probenplan_edit_columns") );
		add_action("manage_posts_custom_column",  array( $this, "probenplan_custom_columns") );
		add_filter('manage_edit-mf_probe_sortable_columns', array( $this, 'my_sortable_cake_column') );
	}

	/**
   * Initialise translations
   */
	public static function load_plugin_textdomain() {
		load_plugin_textdomain('mf_probenplan', false, dirname(plugin_basename(__FILE__)).'/language/');
	}

	/**
     * Load stylesheets
     */
	public function load_admin_stylesheets() {
		global $typenow;

		if ($typenow == 'mf_probe') {
			wp_register_style( 'mf_probenplan_admin_css', plugins_url('/assets/css/admin-styles.css', __FILE__), null, null, false );
			wp_enqueue_style( 'mf_probenplan_admin_css' );
			wp_register_style( 'mf_probenplan_admin_css_datetime', plugins_url('/assets/css/jquery.datetimepicker.css', __FILE__), null, null, false );
			wp_enqueue_style( 'mf_probenplan_admin_css_datetime' );
		}
	}

	/**
     * Load scripts
     */
	public function load_admin_scripts() {
		global $typenow;

		if ($typenow == 'mf_probe') {
			wp_enqueue_script( 'mf_probenplan_admin_js_datetime', plugins_url('/assets/js/jquery.datetimepicker.js', __FILE__) );
			wp_enqueue_script( 'mf_probenplan_admin_js', plugins_url('/assets/js/admin-script.js', __FILE__) );
		}
	}

	/**
	* Registers the new mf_probenplan post-type
	*/
	function register_probe_post_type() {
		$labels =array(
			'name'=> __( 'Rehearsals', 'mf_probenplan' ),
			'singular_name' => __( 'Rehearsal', 'mf_probenplan' ),
			'add_new' => __( 'Add A New Rehearsal', 'mf_probenplan' ),
			'add_new_item' => __( 'Add A New Rehearsal', 'mf_probenplan' ),
			'edit' => __( 'Edit Rehearsal', 'mf_probenplan' ),
			'edit_item' => __( 'Edit Rehearsal', 'mf_probenplan' ),
			'new-item' => __( 'New Rehearsal', 'mf_probenplan' ),
			'view' => __( 'View Rehearsal', 'mf_probenplan' ),
			'view_item' => __( 'View Rehearsal', 'mf_probenplan' ),
			'search_items' => __( 'Search Rehearsals', 'mf_probenplan' ),
			'not_found' => __( 'No Rehearsals Found', 'mf_probenplan' ),
			'not_found_in_trash' => __( 'No Rehearsals Found in Trash', 'mf_probenplan' ),
			'parent' => __( 'Parent Rehearsal', 'mf_probenplan' )
			);

		$args = array(
			'label' => __('Rehearsal', 'mf_probenplan'),
			'description' => __( 'Post Type for Rehearsals', 'mf_probenplan' ),
			'labels' => $labels,
			'show_ui' => true,
			'menu_position' => 6,
			'menu_icon' => 'dashicons-calendar',
			'exclude_from_search' => false,
			'public' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'has_archieve' => true,
			'supports' => array('title', 'editor'),
			'show_in_nav_menus' => true,
			'rewrite' => array ( 'slug' => 'probenplan'),
			'taxonomies' => array('mf_rcategory', 'post_tag')
			);
		/*
		*/
		register_post_type( 'mf_probe' , $args );
	}

	function create_rehearsalcategory_taxonomy() {

		$labels = array(
			'name' => _x( 'Categories', 'taxonomy general name' ),
			'singular_name' => _x( 'Category', 'taxonomy singular name' ),
			'search_items' =>  __( 'Search Categories', 'mf_probenplan' ),
			'popular_items' => __( 'Popular Categories', 'mf_probenplan' ),
			'all_items' => __( 'All Categories', 'mf_probenplan' ),
			'parent_item' => null,
			'parent_item_colon' => null,
			'edit_item' => __( 'Edit Category', 'mf_probenplan' ),
			'update_item' => __( 'Update Category', 'mf_probenplan' ),
			'add_new_item' => __( 'Add New Category', 'mf_probenplan' ),
			'new_item_name' => __( 'New Category Name', 'mf_probenplan' ),
			'separate_items_with_commas' => __( 'Separate categories with commas', 'mf_probenplan' ),
			'add_or_remove_items' => __( 'Add or remove categories', 'mf_probenplan' ),
			'choose_from_most_used' => __( 'Choose from the most used categories', 'mf_probenplan' ),
			);

		register_taxonomy('mf_rcategory', null, array(
			'label' => __('Rehearsal Category', 'mf_probenplan'),
			'labels' => $labels,
			'hierarchical' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'probenkategorie' ),
			));

		register_taxonomy_for_object_type('mf_rcategory', 'mf_probe');
	}

	/**
	* Adds new and removes unnecessary meta-boxes for mf_probe-posts
	*/
	function register_probenplan_meta_boxes() {
		// Add the new role-details box with improved ui and functions:
		add_meta_box('member-details', __('Member Details', 'mf_probenplan'), array( $this, 'output_probenplan_member_meta_box' ), 'mf_probe', 'normal', 'high'); 
		add_meta_box('dates-details', __('Dates Details', 'mf_probenplan'), array( $this, 'output_probenplan_dates_meta_box' ), 'mf_probe', 'normal', 'high'); 
	}

	/**
	* Output a date meta-box
	*
	* @param WP_Post $post WordPress Post object
	*/
	function output_probenplan_dates_meta_box( $post ) {
		/*
		* - Grab data -
		*/
		// Dates:
		$probe_startdate = get_post_meta( $post->ID, 'mf_probenplan_startdate', true );
		$probe_starttime = get_post_meta( $post->ID, 'mf_probenplan_starttime', true );
		$probe_endtime = get_post_meta( $post->ID, 'mf_probenplan_endtime', true );

		if( empty( $probe_startdate ) && $probe_startdate == null ){
			$probe_startdate = time();
		}

		/*
		* - HTML-Output -
		*/ ?>

		<div class="probe-dates">
			<div class="form-block">
				<div class="form-group">
					<label for="mf_probenplan_startdate"><?php _e('Start Date', 'mf_probenplan') ?></label>
					<div class="cal-block">
						<input class="form-control" name="mf_probenplan_startdate" id="mf_probenplan_startdate" class="tfdate" value="<?php echo $probe_startdate; ?>" />
					</div>
				</div>
			</div>
			<div class="form-block">
				<div class="form-group">
					<label for="mf_probenplan_starttime"><?php _e('Start Time', 'mf_probenplan') ?></label>
					<div class="time-block">
						<input class="form-control" name="mf_probenplan_starttime" id="mf_probenplan_starttime" class="tfdate" value="<?php echo $probe_starttime; ?>" />
					</div>
				</div>
				<div class="form-group">
					<label for="mf_probenplan_endtime"><?php _e('End Time', 'mf_probenplan') ?></label>
					<div class="time-block">
						<input class="form-control" name="mf_probenplan_endtime" id="mf_probenplan_endtime" class="tfdate" value="<?php echo $probe_endtime; ?>" />
					</div>
				</div>
			</div>
		</div>

		<?php
	}

	/**
	* Output of a meta-box which combines the mf_casts- and mf_roles-taxonomies
	*
	* @param WP_Post $post WordPress Post object
	*/
	function output_probenplan_member_meta_box ($post) {

		/*
		* - Grab data -
		*/
		// Teilnehmende Rollen:
		$saved_roles = unserialize(get_post_meta( $post->ID, 'mf_probenplan_role_in_cast', true ));

    // Alle Rollen und Besetzungen für die Auswahlliste sammeln:
		$casts = get_terms('mf_casts', array( 'hide_empty' => false, 'orderby' => 'term_order'));
		$roles = $this->get_roles_hierarchic();

		/*
		* Generate a blank cast where portraits should be ordered that belong to no spezific cast
		* 	The term_id is -1
		*/
		$blankcast = new stdClass();
		$blankcast->term_id = (int) -1;
		$blankcast->name = __("All Casts",'mf_probenplan');
		array_push($casts, $blankcast);

		// Generate a wordpress-nonce-field for security:
		wp_nonce_field('save_probenplan_meta', 'probenplan_meta_nonce' );

		/*
		* - HTML-Output -
		*/
		echo '<div class="probe-teilnehmer"><p>';
		echo '<i>' . __( 'Select the roles which should partiziate on this rehearsal. You can select multiple elements.', 'mf_probenplan' ) . '</i>';
		echo '</p>';

		// Casts-Area:
		echo '<div class="casts-area">';

		// Loop through all casts and generate a cast-item box for each:
		foreach ($casts as $cast) {

			// Store the ids of the stored roles in $field['role_ids']:
			if( ! empty( $saved_roles )){
				$field['role_ids'] = $saved_roles[$cast->term_id];
			} else {
				$field['role_ids'] = array();
			}

			// Generate cast-item box, use the wordpress-postbox classes for closing-functionality:
			echo '<div class="cast-item postbox closed">';
			echo '<div class="handlediv" title="Zum Umschalten klicken"><br></div>';
			echo "<h3 class='hndle cast-headline cast-" . $cast->term_id . "'><span>" . $cast->name . "</span></h3>";
			echo '<div class="inside">';

      // Name of all checkboxes in this cast-item, will be transmitted as an array:
			$field['role_name'] = 'mf_roles_'.$cast->term_id.'[]';

      // if no roles are generated only show a notice:
			if(empty($roles)) {
				echo '<p><b>' .__( 'Role: ', 'mf_probenplan' ).'</b><br>'. __("No choices to choose from",'mf_probenplan') . '</p>';
				return false;
			}

      // Loop through all roles and generate a checkbox for each:
			foreach($roles as $arrkey => $role) {
				$key = $role->term_id;
				$value = stripslashes($role->name);

				/*
				* Generate class for the child-level of each term:
				*	level-1 is parent-level of level-2-items, 
				*	level-four is the maximum depth
				*/
				$classes = '';
				if($role->parent != "0"){
					$rp = get_term($role->parent, 'mf_roles');
					if($rp->parent != "0"){
						$rp2 = get_term($rp->parent, 'mf_roles');
						if($rp2->parent != "0"){
							$classes .= ' level-four';
						} else{
							$classes .= ' level-three';
						}
					} else{
						$classes .= ' level-two';
					}
				} else{
					$classes .= ' level-one';
				}

				// Test if this field was stored and check it then:
				$checked = '';
				if(is_array($field['role_ids']) && in_array($key, $field['role_ids'])){
					$checked = 'checked="checked"';
				}
				else{
					if($key == $field['role_ids']) {
						$checked = 'checked="checked"';
					}
				}

				// Output the checkbox with its label:
				echo '<div class="check-group'.$classes.'">';
				echo '<input type="checkbox" value="'.$key.'" id="ck-' . $key . '" name="' . $field['role_name'] . '" '.$checked.'>';
				echo '<label for="ck-' . $key . '">'.$value.'</label>';
				echo '</div>';
			}

			echo '</div>'; // class="inside"
			echo '</div>'; // class="cast-item"
		}

		/*
		* Displays a checkbox which enables the "intelligent-checking" feature (See assets/js). 
		* 	When enabled, at checking a child-category all parent-categories will checked automatically.
		* 	At un-checking a parent category all children will be un-checked, too.
		*/
		echo '<div class="subcontent">';
		echo '<input type="checkbox" value="intelligentCheck" id="intelligentCheck" name="intelligentCheck" checked="checked">';
		echo '<label for="intelligentCheck">'.__("Intelligent Checking Of Parent Roles",'mf_probenplan').'</label>';
		echo '</div>';
		echo '</div>';
		echo '</div>'; // class="cast-area"
	}

	/**
	* Saves the meta box field data
	*
	* @param int $post_id Post ID
	*/
	function save_probenplan_meta_boxes( $post_id ) {
		
		// Check if our nonce is set.
		if ( ! isset( $_POST['probenplan_meta_nonce'] )) {
			return $post_id;
		}

   	// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $_POST['probenplan_meta_nonce'], 'save_probenplan_meta' ) ) {
			return $post_id;
		}

    // Check this is the Probenplan Custom Post Type
		if ( 'mf_probe' != $_POST['post_type'] ) {
			return $post_id;
		}

    // Check the logged in user has permission to edit this post
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}

		if(!isset($_POST["mf_probenplan_startdate"])){
			return $post_id;
		}
    // OK to save meta data

		$updatestartdate = date('Y-m-d', strtotime( $_POST["mf_probenplan_startdate"]) );
		update_post_meta($post_id, "mf_probenplan_startdate", $updatestartdate );

		$updatestarttime = date('G:i', strtotime( $_POST["mf_probenplan_starttime"]) );
		update_post_meta($post_id, "mf_probenplan_starttime", $updatestarttime );

		$updateendtime = date('G:i', strtotime( $_POST["mf_probenplan_endtime"] ) );
		update_post_meta($post_id, "mf_probenplan_endtime", $updateendtime );

		// Get all casts, push blankcast like in the meta-box-generation:
		$casts = get_terms('mf_casts', 'hide_empty=0');
		$blankcast = new stdClass();
		$blankcast->term_id = (int) -1;
		$blankcast->name = __("All Casts",'mf_probenplan');
		array_push($casts, $blankcast);
		
		// Search for $casts-arrays in  $_POST and save in $besetzungen:
		$besetzungen = array();
		foreach ($casts as $cast){
			// Names of the cast-item checkbox groups: mf_roles_ + term_id
			$searchslug = 'mf_roles_'.$cast->term_id;
			if(array_key_exists ( $searchslug, $_POST)){
				$besetzungen[$cast->term_id] = $_POST[$searchslug];
			} else if(array_key_exists ( $searchslug."[]", $_POST)){
				$besetzungen[$cast->term_id] = $_POST[$searchslug];
			}
		}

		// Save array (combination of casts and roles) to mf_probenplan_role_in_cast in post-meta:
		$mfrole = sanitize_text_field( serialize($besetzungen) );
		update_post_meta( $post_id, 'mf_probenplan_role_in_cast', $mfrole );

		// Two arrays for collecting casts-ids and roles-ids where the mf_probe-post belongs to:
		$cast_terms = array();
		$role_terms = array();
		
		foreach ($besetzungen as $keyY => $rollen) {
			array_push($cast_terms, $keyY);
			foreach ($rollen as $keyX => $rolle_id) {
				array_push($role_terms, $rolle_id);
			}
		}
	}

	/**
	* Returns the mf_roles hierarchically, fixes unsorted output of wordpress-get_terms
	*/
	function get_roles_hierarchic () {
		// Get First-Level-Roles:
		$roles_1st = get_terms('mf_roles', array( 'hide_empty' => false, 'orderby' => 'term_order', 'parent' => 0));
		$roles_output = $this->get_role_children(0);

		return $roles_output;
	}

	/**
	* Helper for get_roles_hierarchic(), gets recursively a ordered list of all mf_roles
	*/
	function get_role_children ($role_id){
		$term_children = get_terms('mf_roles', array( 'hide_empty' => false, 'orderby' => 'term_order', 'parent' => $role_id));
		$role_children_output = array();

		if($term_children){
			foreach($term_children as $role_child) {
				array_push($role_children_output, $role_child);
				$role_children_output = array_merge($role_children_output, $this->get_role_children($role_child->term_id));
			}
		}

		return $role_children_output;
	}

	/**
	* Edit colums of mf_probenplan's list-view
	*/
	function probenplan_edit_columns($columns){
		$columns = array(
			"cb" => '<input type="\&quot;checkbox\&quot;" />',
			"mf_probenplan_category" => __("Category", 'mf_probenplan'),
			"title" => __("Name", 'mf_probenplan'),
			"mf_probenplan_dates" => __("Dates", 'mf_probenplan'),
			"mf_probenplan_times" => __("Times", 'mf_probenplan'),
			"id" => __("ID", 'mf_probenplan')
			);

		return $columns;
	}

	/**
	* Spezify whats should be displayed in the columns of mf_probenplan's list-view
	*/
	function probenplan_custom_columns($column){
		global $post;

		switch ($column){
			case "id":
			the_ID();
			break;

			case "mf_probenplan_category":
			   // - show taxonomy terms -
			$eventcats = get_the_terms($post->ID, "mf_rcategory");
			$eventcats_html = array();

			if ($eventcats) {
				foreach ($eventcats as $eventcat){
					array_push($eventcats_html, $eventcat->name);
				}
				echo implode($eventcats_html, ", ");

			} else {
				_e('None', 'mf_probenplan');;
			}
			break;

			case "mf_probenplan_dates":
			// - show dates -
			$probe_startdate = strtotime(get_post_meta( $post->ID, 'mf_probenplan_startdate', true ));
			$startdate = date("d.m.Y", $probe_startdate);
			echo $startdate;
			break;

			case "mf_probenplan_times":
			 // - show times -
			$probe_starttime = strtotime(get_post_meta( $post->ID, 'mf_probenplan_starttime', true ));
			$probe_endtime = strtotime(get_post_meta( $post->ID, 'mf_probenplan_endtime', true ));
			$time_format = get_option('time_format');
			$starttime = date($time_format, $probe_starttime);
			$endtime = date($time_format, $probe_endtime);
			echo $starttime . ' - ' .$endtime;
			break;
		}
	}
	function my_sortable_cake_column( $columns ) {
	    $columns['mf_probenplan_dates'] = 'date';
	 
	    //To make a column 'un-sortable' remove it from the array
	    //unset($columns['date']);
	 
	    return $columns;
	}
}
$wpMF_Probenplan = new MF_Probenplan;
