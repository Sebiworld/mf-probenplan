jQuery(document).ready(function ($) {
	$('#mf_probenplan_startdate').datetimepicker({
		lang:'de',
		i18n:{
			de:{
				months:[
				'Januar','Februar','März','April',
				'Mai','Juni','Juli','August',
				'September','Oktober','November','Dezember',
				],
				dayOfWeek:[
				"So.", "Mo", "Di", "Mi", 
				"Do", "Fr", "Sa.",
				]
			}
		},
		dayOfWeekStart: 1,
		format: 'Y-m-d',
		inline:true,
		timepicker:false,
		scrollMonth: false
	});

	$('#mf_probenplan_starttime').datetimepicker({
		lang:'de',
		i18n:{
			de:{
				months:[
				'Januar','Februar','März','April',
				'Mai','Juni','Juli','August',
				'September','Oktober','November','Dezember',
				],
				dayOfWeek:[
				"So.", "Mo", "Di", "Mi", 
				"Do", "Fr", "Sa.",
				]
			}
		},
		step: 15,
		format: 'G:i',
		datepicker:false,
		inline:true,
		defaultTime: '18:00'
	});
	$('#mf_probenplan_endtime').datetimepicker({
		lang:'de',
		i18n:{
			de:{
				months:[
				'Januar','Februar','März','April',
				'Mai','Juni','Juli','August',
				'September','Oktober','November','Dezember',
				],
				dayOfWeek:[
				"So.", "Mo", "Di", "Mi", 
				"Do", "Fr", "Sa.",
				]
			}
		},
		step: 15,
		format: 'G:i',
		datepicker:false,
		inline:true,
		defaultTime: '22:00'
	});

	$(".check-group.level-two :checkbox, .check-group.level-three :checkbox, .check-group.level-four :checkbox").on('click', function() {
		if ($('#intelligentCheck:checkbox').is(':checked') && !$(this).is(':checked')) {
			$(this).uncheckParent();
		}
	});

	$(".check-group.level-one :checkbox, .check-group.level-two :checkbox, .check-group.level-three :checkbox").on('click', function() {
		if ($('#intelligentCheck:checkbox').is(':checked') && $(this).is(':checked')) {
			$(this).checkChildren();
		}
	});

	

	// unCheckt bei Elementen aus tieferen Ebenen alle Elternelemente
	$.fn.uncheckParent = function() {
		var this_checkgroup = $(this).parent();
		var parent_checkgroup = this_checkgroup;
		while(!$(this_checkgroup).hasClass('level-one') && ($(this_checkgroup).hasClass('level-two') || $(this_checkgroup).hasClass('level-three') || $(this_checkgroup).hasClass('level-four'))){
			if($(this_checkgroup).hasClass('level-two')){
				// So lange weiter durchlaufen bis kein Geschwisterelement mehr da ist:
				while($(parent_checkgroup).hasClass('level-two') || $(parent_checkgroup).hasClass('level-three') || $(parent_checkgroup).hasClass('level-four')){
					parent_checkgroup = $(parent_checkgroup).prev();
				}
				// Wenn jetzt ein Elternelement gefunden wird hat alles geklappt:
				if($(parent_checkgroup).hasClass('level-one')){
					parent_checkgroup.find(':checkbox').prop('checked', false);
				} else {
					// Fehler, Methode stoppen
					return this;
				}
			} else if($(this_checkgroup).hasClass('level-three')){
				// So lange weiter durchlaufen bis kein Geschwisterelement mehr da ist:
				while($(parent_checkgroup).hasClass('level-three') || $(parent_checkgroup).hasClass('level-four')){
					parent_checkgroup = $(parent_checkgroup).prev();
				}
				// Wenn jetzt ein Elternelement gefunden wird hat alles geklappt:
				if($(parent_checkgroup).hasClass('level-two')){
					parent_checkgroup.find(':checkbox').prop('checked', false);
				} else {
					// Fehler, Methode stoppen
					return this;
				}
			} else if($(this_checkgroup).hasClass('level-four')){
				// So lange weiter durchlaufen bis kein Geschwisterelement mehr da ist:
				while($(parent_checkgroup).hasClass('level-four')){
					parent_checkgroup = $(parent_checkgroup).prev();
				}
				// Wenn jetzt ein Elternelement gefunden wird hat alles geklappt:
				if($(parent_checkgroup).hasClass('level-three')){
					parent_checkgroup.find(':checkbox').prop('checked', false);
				} else {
					// Fehler, Methode stoppen
					return this;
				}
			} else {
				// Fehler, Methode stoppen
				return this;
			}

			this_checkgroup = parent_checkgroup;
		}
		return this;
	};

	// Checkt alle Kinder wenn ein Elternelement gecheckt wird
	$.fn.checkChildren = function() {
		var this_checkgroup = $(this).parent();
		var child_checkgroup = $(this_checkgroup).next();
		
		if($(this_checkgroup).hasClass('level-three')){
			while($(child_checkgroup).hasClass('level-four')){
				child_checkgroup.find(':checkbox').prop('checked', true);
				child_checkgroup = $(child_checkgroup).next();
			}
		} else if($(this_checkgroup).hasClass('level-two')){
			while($(child_checkgroup).hasClass('level-three') || $(child_checkgroup).hasClass('level-four')){
				child_checkgroup.find(':checkbox').prop('checked', true);
				child_checkgroup = $(child_checkgroup).next();
			}
		} else if($(this_checkgroup).hasClass('level-one')){
			while($(child_checkgroup).hasClass('level-two') || $(child_checkgroup).hasClass('level-three') || $(child_checkgroup).hasClass('level-four')){
				child_checkgroup.find(':checkbox').prop('checked', true);
				child_checkgroup = $(child_checkgroup).next();
			}
		}

		return this;
	};

});
